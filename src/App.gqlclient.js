/**
 * @author Kumaravel Pazhani
 * @email kumaravel@qdmplatforms.com
 * @create 12/9/2020
 * @modify 12/11/2020 
 * @desc GraphQL client setup done here using the Apollo Client.
 */

import React from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloGQLClient from "./graphql";

const AppGQLClient = (props) => {
  return (
    <ApolloProvider client={ApolloGQLClient}>{props.children}</ApolloProvider>
  );
};

export default AppGQLClient;
