/**
 * @author Kumaravel Pazhani
 * @email kumaravel@qdmplatforms.com
 * @create 12/12/202 
 * @modify 12/12/202 
 * @desc Providing the ThemeContext from /src/context which is used in /src/App.js
 */

 export { default as SectionContext } from './sectionContext';
