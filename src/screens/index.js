/**
 * @author Sri Rahul
 * @email rahul@qdmplatforms.com
 * @create date 2021-02-24 14:51:22
 * @modify date 2021-02-24 14:51:22
 * @desc [description]
 */


export { default as Login } from "./login";
export { default as NotFound } from "./notFound";
export * from "./section";
