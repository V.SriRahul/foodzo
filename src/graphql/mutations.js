/**
 * @author Kumaravel Pazhani
 * @email kumaravel@qdmplatforms.com
 * @create 12/9/2020 
 * @modify 12/11/2020 
 * @desc Collection of all graphQL mutations
 */

import { gql } from "apollo-boost";

const mutations = {};

export default mutations;
