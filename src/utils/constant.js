const Types = [
    // { label: "Integer", value: 1 },
    // { label: "BigInt", value: 2 },
    // { label: "String", value: 3 },
    // { label: "Float", value: 4 },
    // { label: "Double", value: 5 },
    // { label: "Decimal", value: 6 },
    // { label: "LOV", value: 'lov' }
    { label:"Boolean",value:1 },
    { label:"DigitalObject",value:2 },
    { label:"Reference",value:3 },
    { label:"String",value:4 },
    { label:"Date",value:5 },
    { label:"DateTime",value:6 },
    { label:"Decimal",value:7 },
    { label:"Instant",value:8 },
    { label:"Integer",value:9 },
    { label:"PositiveInt",value:10 },
    { label:"Time",value:11 },
    { label:"UnsignedInt",value:12 },
    { label:"Dimension",value:13},
     { label: "LOV", value: 'lov' }

]

const securityData = [
    { label: 'Encryption', value: 'Encryption' },
    { label: 'Sensitive information', value: 'Sensitive_Information' },
    { label: 'Personal information', value: 'Personal_information' }
]

const securityState = {
    Encryption: false,
    Sensitive_Information: false,
    Personal_information: false,
}


const typeData = [
    { label: 'Indexed', value: 'indexed' },
    { label: 'Masked', value: 'masked' },
    { label: 'Unique', value: 'unique' },
    { label: 'Auto Incriment', value: 'autoIncrement' }
]

const typeDataState = {
    indexed: false,
    masked: false,
    unique: false,
    autoIncrement: false
}

const digitalObjectButton = [
    { label: 'PDF', value: 'pdf' },
    { label: 'Word', value: 'word' },
    { label: 'Video', value: 'video' },
    { label: 'JPEG', value: 'jpeg' },
    { label: 'Other', value: 'other' },
    { label: 'LOV', value: 'lov' }
]

const digitalObjectList = [
    { label: 'Readable / OCRable', value: 'readableOrOcrable' },
    { label: 'Compression', value: 'compression' },
    { label: 'Thumnail', value: 'Thumbnail' },
]

const digitalObjectListState = {
    compression: false,
    Thumbnail: false,
    readableOrOcrable: false
}
export {
    Types,
    securityData,
    typeData,
    digitalObjectButton,
    digitalObjectList,
    securityState,
    typeDataState,
    digitalObjectListState
}